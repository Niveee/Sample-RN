//Importing libraries

import React from 'react';
import { Text,
          View
       } from 'react-native';

//Component
const styles = {
    viewStyle:{
        backgroundColor: '#bababa',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        paddingTop: 15,
        shadowColor: '#a2a3a5',
        shadowOffset: { width: 0, height: 20 },
        shadowOpacity: 1,
        elevation: 2,
        position: 'relative'
    },
    textStyle:{
        fontSize: 15
    }
};

export default class Header extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { textStyle,viewStyle } = styles;
        return (
        <View style={viewStyle}>
            <Text style={textStyle} >{ this.props.headerText }</Text>
        </View>
        );
    }
};

